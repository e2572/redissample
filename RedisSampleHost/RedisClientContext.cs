﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
namespace RedisSampleHost
{
    public static class RedisClientContext
    {
        private const string redisConnectionString = "127.0.0.1:6379,password=Abc123";
        public static ConnectionMultiplexer ConnectionMultiplexer = ConnectionMultiplexer.Connect(redisConnectionString);
        public static IDatabase Database = ConnectionMultiplexer.GetDatabase();
    }
}
