﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace RedisSampleHost.DataTypeSample
{
    public class StringTest
    {
        public StringTest() { }
        /// <summary>
        /// 比如计算访问次数、点赞、转发、库存数量等等。
        /// </summary>
        public void IncrementInMultiTask()
        {
            var key = "String_Increment";
            RedisClientContext.Database.StringGetDelete(key);
            var tasks = new Task[5];
            for (int i = 0; i < tasks.Count(); i++)
            {
                tasks[i] = Task.Run(() =>
                {
                    for (int k = 0; k < 100; k++)
                    {
                        RedisClientContext.Database.StringIncrement(key);
                    }

                });
            }
            Task.WaitAll(tasks);

            // the result is 500
            Console.WriteLine($"String_Increment is {RedisClientContext.Database.StringGet(key)}");
        }

        /// <summary>
        /// 分布式锁
        /// </summary>
        public void LockTakeInMultiTask()
        {
            string lockKey = "String_LockTake";
            var tasks = new Task[5];
            var count = 0;
            for (int i = 0; i < tasks.Count(); i++)
            {
                tasks[i] = Task.Factory.StartNew((obj) =>
                {
                    var currentLockFlag = false;
                    while (true)
                    {
                        if (RedisClientContext.Database.LockTake(lockKey, count, TimeSpan.FromSeconds(5)))
                        {
                            try
                            {
                                //if delay time more than 5 second
                                Task.Delay(1000).Wait();

                                Console.WriteLine($"{DateTime.Now} >> task {obj} entered..");
                                for (int i = 0; i < 100; i++)
                                {
                                    count++;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            finally
                            {
                                RedisClientContext.Database.LockRelease(lockKey, count);
                                currentLockFlag = true;
                            }
                        }

                        if (currentLockFlag)
                        {
                            break;
                        }

                        Task.Delay(200).Wait();
                    }
                },i);
            }
            Task.WaitAll(tasks);

            Console.WriteLine($"CountKey value is {count} by LocaTake");
        }

        public void LockTakeExpiry()
        {
            var lockKey = "LockTake_Expiry";
            var lockTakeStatus = RedisClientContext.Database.LockTake(lockKey, 1, TimeSpan.FromSeconds(3));
            Console.WriteLine($"{DateTime.Now} >> Take the lock,expiry time is 3s,current status is {lockTakeStatus}");
            Thread.Sleep(1 * 1000);
            lockTakeStatus = RedisClientContext.Database.LockTake(lockKey, 1, TimeSpan.FromSeconds(3));
            Console.WriteLine($"{DateTime.Now} >>After 1s,current status is {lockTakeStatus}");
            Thread.Sleep(2 * 1000);
            lockTakeStatus = RedisClientContext.Database.LockTake(lockKey, 1, TimeSpan.FromSeconds(3));
            Console.WriteLine($"{DateTime.Now} >>After 3s,current status is {lockTakeStatus}");

            //key 过期后, 会立即自动被删除掉
        }

        public void Set()
        {
            // single value
            RedisClientContext.Database.StringSet("Single_Value", "hello redis.");

            //object value
            var testObj = new { Name = "sss", Age = 18 };
            RedisClientContext.Database.StringSet("Object_Value", JsonConvert.SerializeObject(testObj));

            //key value (MSet)
            var dic = new Dictionary<string, int>()
            {
                { "Cat",1 },
                { "Dot",2}
            };
            //RedisClientContext.Database.StringSet(dic,When.Always, CommandFlags.None);
        }
    }
}
