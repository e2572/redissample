﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisSampleHost.DataTypeSample
{
    public class ListTest
    {
        private const string QueueKey = "Message_Key";
        private const string NoticeKey = "Notice_Key";
        private readonly ISubscriber _subscriber;
        private readonly RedisChannel _channelWithLiteral;
        public ListTest() 
        {
            _subscriber = RedisClientContext.ConnectionMultiplexer.GetSubscriber();
            _channelWithLiteral = new RedisChannel(NoticeKey, RedisChannel.PatternMode.Literal);
        }

        /// <summary>
        /// 非阻塞队列 (pub/sub)
        /// </summary>
        public void MessageQueueWithUnBlock()
        {
            // push
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    var rd = new Random();
                    Thread.Sleep(rd.Next(1, 1) * 1000);
                    RedisClientContext.Database.ListLeftPush(QueueKey, DateTime.Now.ToString());
                    _subscriber.Publish(_channelWithLiteral, string.Empty);
                }
            }, TaskCreationOptions.LongRunning);

            Task.Factory.StartNew(() =>
            {
                // pop
                // 这里是异步处理的，不会阻塞
                _subscriber.Subscribe(_channelWithLiteral, (channel, message) =>
                {
                    ProcessMessage();
                });
            });


            void ProcessMessage()
            {
                // 如果处理速度慢,很耗CPU
                Thread.Sleep(3000);
                var value = RedisClientContext.Database.ListRightPop(QueueKey);
                Console.WriteLine($"Current thread is {Thread.CurrentThread.ManagedThreadId}, Dequeue value is {value}, queue length is {RedisClientContext.Database.ListLength(QueueKey)}");
            }
        }

        /// <summary>
        /// 阻塞式队列 (loop)
        /// </summary>
        public void MessageQueueWithBlock()
        {
            // push
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    var rd = new Random();
                    Thread.Sleep(rd.Next(1, 1) * 1000);
                    RedisClientContext.Database.ListLeftPush(QueueKey, DateTime.Now.ToString());
                }
            }, TaskCreationOptions.LongRunning);

            while (true)
            {
                while (RedisClientContext.Database.ListLength(QueueKey) > 0)
                {
                    var value = RedisClientContext.Database.ListRightPop(QueueKey);
                    Console.WriteLine($"Current thread is {Thread.CurrentThread.ManagedThreadId}, Dequeue value is {value}, queue length is {RedisClientContext.Database.ListLength(QueueKey)}");
                }  
                
                Thread.Sleep(3000);
            }
        }
    }
}
