﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisSampleHost.DataTypeSample
{
    public class HashTest
    {
        public HashTest() { }

        public void HashSet()
        {
            // set name
            RedisClientContext.Database.HashSet("Hash_Key", "Hash_Key_Name", "Jack");
        }

    }
}
