﻿using RedisSampleHost.DataTypeSample;

namespace RedisSampleHost
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //StringTestConsole();
            ListTestConsole();
            Console.ReadKey();
        }

        static void StringTestConsole()
        { 
            //new StringTest().IncrementInMultiTask();
            //new StringTest().LockTakeInMultiTask();
            new StringTest().LockTakeExpiry();
        }

        static void ListTestConsole()
        {
            //new ListTest().MessageQueueWithUnBlock();
            new ListTest().MessageQueueWithBlock();
        }
    }
}